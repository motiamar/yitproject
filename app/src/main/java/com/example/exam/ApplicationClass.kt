package com.example.exam

import android.app.Application
import android.content.Context
import com.example.exam.di.ViewModelModule
import com.example.exam.di.appDB
import com.example.exam.di.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ApplicationClass : Application() {

    companion object {

        lateinit var appContext: Context
        lateinit var mApplication: Application

    }

    override fun onCreate() {
        super.onCreate()
        mApplication = this
        ApplicationClass.appContext = applicationContext
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@ApplicationClass)
            modules(listOf(ViewModelModule, retrofitModule, appDB))
        }
    }


}