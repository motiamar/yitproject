package com.example.exam.di

import android.app.Application
import androidx.room.Room
import com.example.exam.constants.AppConstants
import com.example.exam.db.dao.CharactersDao
import com.example.exam.db.dbLocalFiles.AppRoomDataBase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val appDB = module {
    fun provideDataBase(application: Application): AppRoomDataBase {
        return  Room.databaseBuilder(application, AppRoomDataBase::class.java, AppConstants.CHARACTERS_TABLE_NAME)
            .fallbackToDestructiveMigration().allowMainThreadQueries()
            .build()
    }

    fun provideCharacterDao(dataBase: AppRoomDataBase): CharactersDao {
        return dataBase.characterDao
    }

    single { provideDataBase(androidApplication()) }

    single { provideCharacterDao(get()) }

}
