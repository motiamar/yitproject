package com.example.exam.di


import com.example.exam.iu.charactersFragment.CharacterServerDataSource
import com.example.exam.iu.charactersFragment.CharactersFragmentViewModel
import com.example.exam.iu.charactersFragment.CharactersRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ViewModelModule = module {

    single {
        CharacterServerDataSource(get())
    }

    single {
        CharactersRepository(get(), get())
    }

    viewModel {
        CharactersFragmentViewModel(get())
    }




}