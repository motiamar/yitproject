package com.example.exam.di

import com.example.exam.apiServices.CharacterServicesAPI
import com.example.exam.constants.NetworkConstants
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

var retrofitModule = module{

    single {
        retrofit()
    }

    single {
        get<Retrofit>().create(CharacterServicesAPI::class.java)
    }

}


private fun retrofit() = Retrofit.Builder()
    .callFactory(
        OkHttpClient.Builder()
        .build())
    .baseUrl(NetworkConstants.APP_BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()



