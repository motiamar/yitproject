package com.example.exam.apiServices

import com.example.exam.db.entities.CharacterDBItem
import com.example.exam.db.entities.CharacterListModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CharacterServicesAPI {

    @GET("character/{characterId}")
    fun getCharacterById(@Path("characterId") aCharacterId: Int): CharacterDBItem

    @GET("character")
    fun getAllCharacters(): Call<CharacterListModel>

}