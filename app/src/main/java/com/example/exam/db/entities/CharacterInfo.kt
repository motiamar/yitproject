package com.example.exam.db.entities

data class CharacterInfo(
    val pages: Int,
    val count: Int,
    val prev: Int,
    val next: String
)