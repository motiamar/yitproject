package com.example.exam.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.exam.constants.AppConstants

@Entity(tableName = AppConstants.CHARACTERS_TABLE_NAME)
data class CharacterDBItem(

    @PrimaryKey
    val id: Int,

    //Character details.
    val image: String,
    val gender: String,
    val type: String,
    val name: String,
    val species: String,
    val status: String
)