package com.example.exam.db.entities

data class CharacterListModel(
    val info: CharacterInfo,
    val results: List<CharacterDBItem>
)