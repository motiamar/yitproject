package com.example.exam.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.exam.db.entities.CharacterDBItem

@Dao
interface CharactersDao {

    @Query("SELECT * FROM charactersTable")
    fun getAllCharacters() : List<CharacterDBItem>

    @Query("SELECT * FROM charactersTable WHERE id = :CharactersId")
    fun getCharacterById(CharactersId: Int): CharacterDBItem

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllCharacters(characters: List<CharacterDBItem>)

}