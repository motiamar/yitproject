package com.example.exam.db.dbLocalFiles

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.exam.db.dao.CharactersDao
import com.example.exam.db.entities.CharacterDBItem

@Database(entities = [CharacterDBItem::class], version = 1)
abstract class AppRoomDataBase : RoomDatabase(){
    abstract val characterDao: CharactersDao
}
