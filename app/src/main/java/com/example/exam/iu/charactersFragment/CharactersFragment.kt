package com.example.exam.iu.charactersFragment

import android.os.Bundle
import androidx.lifecycle.Observer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.exam.R
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_GENDER
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_IMAGE
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_NAME
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_TYPE
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_SPECIES
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_STATUS
import com.example.exam.db.entities.CharacterDBItem
import com.example.exam.iu.adapters.CharactersAdapter
import com.example.exam.iu.adapters.RecyclerItemClickListener
import kotlinx.android.synthetic.main.main_characters_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class CharactersFragment : Fragment() {

    private val viewModel: CharactersFragmentViewModel by viewModel()

    private lateinit var mCharactersAdapter: CharactersAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_characters_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initCharactersRecyclerView()
        setupObservers()

        viewModel.getAllCharacters()

    }

    private fun initCharactersRecyclerView() {
        mCharactersAdapter = CharactersAdapter(requireContext(), arrayListOf<CharacterDBItem>())
        characters_rv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mCharactersAdapter
        }

        onCharactersClicked()
    }

    private fun onCharactersClicked() {
        characters_rv.addOnItemTouchListener(
            RecyclerItemClickListener(
                context,
                characters_rv,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View?, position: Int) {

                        val item = mCharactersAdapter.getItemAtPosition(position)
                        val bundle = Bundle()
                        bundle.putString(BUNDLE_KEY_NAME, item.name)
                        bundle.putString(BUNDLE_KEY_STATUS, item.status)
                        bundle.putString(BUNDLE_KEY_SPECIES, item.species)
                        bundle.putString(BUNDLE_KEY_GENDER, item.gender)
                        bundle.putString(BUNDLE_KEY_IMAGE, item.image)
                        bundle.putString(BUNDLE_KEY_TYPE, item.type)

                        findNavController().navigate(
                            R.id.action_charactersFragment_to_characterDetailFragment,
                            bundle
                        )
                    }

                    override fun onLongItemClick(view: View?, position: Int) {
                        // do whatever
                    }
                })
        )
    }

    private fun setupObservers() {
        viewModel.getAllCharacters()!!.observe(viewLifecycleOwner, Observer {
            mCharactersAdapter.apply {
                if (it != null) {
                    mCharactersAdapter.updateValues(it)
                }
            }
        })

    }

}
