package com.example.exam.iu.charactersFragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.exam.db.entities.CharacterDBItem

class CharactersFragmentViewModel constructor(private val mCharactersRepository: CharactersRepository) : ViewModel(){

    private var mAllCharactersLiveData = MutableLiveData<List<CharacterDBItem>>()

    init {
        mCharactersRepository.getAllCharacters(mAllCharactersLiveData)
    }

    fun getAllCharacters(): LiveData<List<CharacterDBItem>>? {
        return mAllCharactersLiveData
    }
}