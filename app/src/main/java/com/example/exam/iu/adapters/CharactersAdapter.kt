package com.example.exam.iu.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.example.exam.R
import com.example.exam.db.entities.CharacterDBItem

class CharactersAdapter(
    private val mContext: Context,
    var mListItemsData: List<CharacterDBItem>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private inner class CharacterViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var characterImageView: AppCompatImageView = itemView.findViewById(R.id.iv_character_image)
        var characterName: TextView = itemView.findViewById(R.id.tv_character_name)
        //var characterType: TextView = itemView.findViewById(R.id.tv_character_type)

        fun doOnBindItem(position: Int) {
            doItemBinding(position)
        }

        private fun doItemBinding(position: Int) {
            val characterItem = mListItemsData[position]

            characterName.text = characterItem.name
            //characterType.text = characterItem.species

            Glide.with(mContext)
                .load(characterItem.image)
                .transform(CircleCrop())
                .into(characterImageView)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return CharacterViewHolder(
            LayoutInflater.from(mContext).inflate(R.layout.character_rv_item, parent, false)
        )

    }

    override fun getItemCount(): Int {
        return mListItemsData.size
    }

    fun getItemAtPosition(position: Int): CharacterDBItem = mListItemsData[position]


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CharacterViewHolder).doOnBindItem(position)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateValues(values: List<CharacterDBItem>){
        mListItemsData = values
        notifyDataSetChanged()
    }




}