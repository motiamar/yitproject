package com.example.exam.iu.charactersFragment

import com.example.exam.db.dao.CharactersDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import androidx.lifecycle.MutableLiveData
import com.example.exam.db.entities.CharacterDBItem
import com.example.exam.db.entities.CharacterListModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CharactersRepository constructor(private val mCharacterServerDataSource: CharacterServerDataSource,
                                        private val mCharactersDao: CharactersDao) {


    fun getAllCharacters(mAllCharactersLiveData: MutableLiveData<List<CharacterDBItem>>?){

        getAllCharactersFromServer(mAllCharactersLiveData)
        mAllCharactersLiveData!!.postValue(mCharactersDao.getAllCharacters())

    }


    private fun getAllCharactersFromServer(mAllCharactersLiveData: MutableLiveData<List<CharacterDBItem>>?) {
        CoroutineScope(IO).launch {
            val call = mCharacterServerDataSource.getAllCharacters()
            call.enqueue(object : Callback<CharacterListModel?> {
                override fun onResponse(call: Call<CharacterListModel?>, response: Response<CharacterListModel?>) {
                    mAllCharactersLiveData!!.postValue(response.body()!!.results)
                    saveCharactersListOnDB(response.body()!!)
                }

                override fun onFailure(call: Call<CharacterListModel?>, t: Throwable) {
                    // todo deal with the failed network request
                }
            })
        }
    }


    private fun saveCharactersListOnDB(allCharacters: CharacterListModel) {
        CoroutineScope(IO).launch {
            mCharactersDao.insertAllCharacters(allCharacters.results)
        }
    }


}