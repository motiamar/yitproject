package com.example.exam.iu.charactersDetails

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.example.exam.R
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_GENDER
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_IMAGE
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_NAME
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_TYPE
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_SPECIES
import com.example.exam.constants.AppConstants.Companion.BUNDLE_KEY_STATUS
import kotlinx.android.synthetic.main.character_detail_fragment.*


class CharactersDetailsFragment : Fragment(){


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.character_detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()

    }

    private fun initViews() {
        var name = ""
        arguments?.getString(BUNDLE_KEY_NAME)?.let {
            name = it
            character_name.text = it
        }

        arguments?.getString(BUNDLE_KEY_STATUS)?.let {
            character_status.text = it
        }
        arguments?.getString(BUNDLE_KEY_SPECIES)?.let {
            species.text = it
        }
        arguments?.getString(BUNDLE_KEY_GENDER)?.let {
            character_gender.text = it
        }
        arguments?.getString(BUNDLE_KEY_TYPE)?.let {
            character_type_text.text = it
        }

        arguments?.getString(BUNDLE_KEY_IMAGE)?.let {
            Glide.with(this)
                .load(it)
                .transform(CircleCrop())
                .into(character_image)
        }

        c_share.setOnClickListener {
            shareCharacter(name)
        }
    }



    fun shareCharacter(name: String) {
        val intent = Intent(Intent.ACTION_SEND)
        val shareBody = "Hi, Please meet $name"
        intent.type = "text/plain"
        intent.putExtra(
            Intent.EXTRA_SUBJECT,
            getString(R.string.share_subject)
        )
        intent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(intent, "${getString(R.string.share_using)}  $name"))
    }
}
