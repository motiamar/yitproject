package com.example.exam.iu.charactersFragment

import com.example.exam.apiServices.CharacterServicesAPI
import com.example.exam.db.entities.CharacterListModel
import retrofit2.Call

class CharacterServerDataSource constructor(private val mCharacterApi: CharacterServicesAPI) {

    fun getCharacterById(aCharacterId: Int) =  mCharacterApi.getCharacterById(aCharacterId)

    fun getAllCharacters() : Call<CharacterListModel> = mCharacterApi.getAllCharacters()

}