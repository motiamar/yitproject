package com.example.exam.constants

class AppConstants {

    companion object{

        const val CHARACTERS_TABLE_NAME = "charactersTable"

        const val BUNDLE_KEY_NAME = "name"
        const val BUNDLE_KEY_STATUS = "status"
        const val BUNDLE_KEY_SPECIES = "species"
        const val BUNDLE_KEY_GENDER = "gender"
        const val BUNDLE_KEY_IMAGE = "image"
        const val BUNDLE_KEY_TYPE = "type"

    }
}